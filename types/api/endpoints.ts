export enum ApiEndpoint {
  Book = '/books',
  BookRelated = '/books/related',
  Serie = '/series',
  SerieBook = '/series/books',
  Author = '/authors',
  AuthorBook = '/authors/books',
}
