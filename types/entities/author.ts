export interface AuthorMeta {
  slug?: string
  show?: string
}

export interface Author {
  name?: string
  meta?: AuthorMeta
}
